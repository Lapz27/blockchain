use blockchain::CHAIN;
use rocket_contrib::{Json, Value};
use serde_json;
use rocket::config::Config;
use rocket::Rocket;
use rocket;

#[derive(Debug, Deserialize)]
struct Seed {
    seed: String,
}

#[get("/", format = "application/json")]
fn index() -> String {

    let chain = CHAIN.lock().unwrap();

    let chain_str = serde_json::to_string(&*chain).unwrap();

    chain_str
}



#[post("/mine", format = "application/json", data = "<seed>")]
fn mine(seed: Json<Seed>) -> Json<Value> {


    info!("seed:{:?}", seed);

    let seed = seed.0.seed;

    let mut chain = CHAIN.lock().expect("Locked");
    let response = match chain.mine(seed) {
        Ok( () ) => {
            Json(json!({
           "message":"Block mined"
            }))
        },
        Err(_) => Json(json!({"message":"Failed to mine Block"})),
    };

    response
}

pub fn start_http_server(config: Config) -> Rocket {
    //     rocket::Rocket::custom(config,false).mount("/", routes![index,mine])
    rocket::custom(config, false).mount("/", routes![index,mine])

}
