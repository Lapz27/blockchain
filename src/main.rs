#![feature(plugin)]
#![plugin(rocket_codegen)]


extern crate rocket;
extern crate serde;
#[macro_use]
extern crate serde_json;

extern crate rocket_contrib;

#[macro_use]
extern crate serde_derive;
extern crate url;
extern crate env_logger;


#[macro_use]
extern crate log;

#[macro_use]
extern crate lazy_static;

extern crate chrono;
extern crate crypto;
extern crate websocket;
extern crate clap;
extern crate ws;


mod blockchain;
mod server;
mod p2p;

// use server::{Client,P2PServer};
use clap::{Arg, App, AppSettings, SubCommand};
use server::start_http_server;
use p2p::start_server;
use std::thread;

use rocket::config::{Config, Environment};

fn main() {




    env_logger::init().unwrap();
    // thread::spawn(|| ));

    // rocket().launch();

    let matches = App::new("Simple Blockchain")
        .setting(AppSettings::ArgRequiredElseHelp)
        .version("0.2")
        .author("Lenard P. <striderman34@gmail.com>")
        .subcommand(SubCommand::with_name("http")
                        .about("Runs a http server connected to the blockchain")
                        .author("Lenard P. <striderman34@gmail.com>")
                        .arg(Arg::with_name("address"))
                        .arg(Arg::with_name("port")))
        .subcommand(SubCommand::with_name("ws")
                        .about("Runs a blockchain listening on a websocket port")
                        .author("Lenard P. <striderman34@gmail.com>")
                        .arg(Arg::with_name("address"))
                        .arg(Arg::with_name("port")))
        .arg(Arg::with_name("server")
                 .short("s")
                 .value_name("SERVER")
                 .help("Set the address to listen for new connections."))
        .arg(Arg::with_name("peer")
                 .takes_value(true)
                 .value_name("PEER")
                 .help("A WebSocket URL to attempt to connect to at start.")
                 .multiple(true))
        .get_matches();

    match matches.subcommand() {
        ("http", Some(sub_input)) => {

            let address = sub_input.value_of("address").unwrap();
            let port = sub_input
                .value_of("port")
                .unwrap()
                .parse::<u16>()
                .unwrap();

            let config = Config::build(Environment::Staging)
                .address(address)
                .port(port)
                .finalize()
                .unwrap();

            let rocket = start_http_server(config);

            rocket.launch();

        },

        ("ws", Some(sub_input)) => {
            let address = sub_input.value_of("address").unwrap();
            let port = sub_input.value_of("port").unwrap();

            start_server(format!("{}:{}", address, port));
        },

        _ => error!("Invalid subcommand"),

    }

    let sockets =
        thread::spawn(move || if let Some(address) = matches.clone().value_of("server") {

                          let mut socket =
                              ws::WebSocket::new(|_| move |msg| Ok(info!("Got message:{}", msg)))
                                  .unwrap();

                          if let Some(peers) = matches.clone().values_of("peer") {

                              for peer in peers {

                                  socket.connect(url::Url::parse(peer).unwrap()).unwrap();
                              }
                          }

                          socket.listen(address).unwrap();

                      });

    sockets.join().unwrap();








    // if matches.is_present("server") {

    //     let address = matches.value_of("server_address");

    //     // let mut server = P2PServer::new();

    //     // server.start_server(address);
    // }

    // if let Some(peers) = matches.values_of("PEER") {
    //     let client = Client::new();

    //     for peer in peers {
    //         client.connect_to(peer);
    //     }
}
