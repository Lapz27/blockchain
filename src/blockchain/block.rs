use chrono::{DateTime, Utc};
use std::cmp::Ordering;


#[derive(Debug, Clone, Eq, Serialize, Deserialize)]
pub struct Block {
    pub index: i64,
    pub timestamp: DateTime<Utc>,
    pub data: String,
    pub hash: String,
    pub previous_hash: String,
    pub nonce: i64,
}


impl Ord for Block {
    fn cmp(&self, other: &Block) -> Ordering {
        self.index.cmp(&other.index)
    }
}

impl PartialOrd for Block {
    fn partial_cmp(&self, other: &Block) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Block {
    fn eq(&self, other: &Block) -> bool {
        self.index == other.index
    }
}
impl Block {
    pub fn new(
        index: i64,
        previous_hash: String,
        timestamp: DateTime<Utc>,
        data: String,
        hash: String,
        nonce: i64,
    ) -> Self {
        Block {
            index,
            previous_hash,
            timestamp,
            hash,
            data,
            nonce,
        }
    }
}
