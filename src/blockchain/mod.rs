
// TODO stops lots of clones
mod block;

use self::block::Block;

use chrono::{DateTime, Utc, TimeZone};
use crypto::sha3::Sha3;
use crypto::digest::Digest;
use std::sync::Mutex;
#[derive(Debug,Serialize)]
pub enum Error {
    Mine,
    ShorterChain,
    InvalidChain,
    InvalidBlock,
    InvalidHash,
    InvalidIndex,
    HashNotDifficult,
}
lazy_static! {
    pub static ref CHAIN: Mutex<BlockChain> = Mutex::new(BlockChain::new());
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct BlockChain {
    pub blocks: Vec<Block>,
    difficulty: i64,
}

fn genesis() -> Block {
    Block::new(0,
               "0".to_owned(),
               Utc.timestamp(1465154705, 0),
               "my genesis block!!".to_owned(),
               "816534932c2b7154836da6afc367695e6337db8a921823784c14378abed4f7d7".to_owned(),
               56551)
}

impl BlockChain {
    pub fn new() -> Self {
        BlockChain {
            difficulty: 4,
            blocks: vec![genesis()],
        }
    }


    pub fn get_latest_block(&self) -> &Block {
        &self.blocks[self.blocks.len() - 1]
    }

    pub fn mine(&mut self, seed: String) -> Result<(), Error> {
        let new_block = self.generate_next_block(seed);
        if self.add_block(new_block)? {
            info!("Block mined");
            return Ok(());
        }

        Err(Error::Mine)
    }



    pub fn replace_chain(&mut self, new_blocks: Vec<Block>) -> Result<(), Error> {

        if !self.is_valid_chain(&new_blocks)? {
            println!("❌ Replacement chain is not valid. Won't replace existing blockchain.");

            return Err(Error::InvalidChain);
        }

        if new_blocks.len() <= self.blocks.len() {
            println!("❌  Replacement chain is shorter than original. Won't replace existing blockchain.");

            return Err(Error::ShorterChain);
        }

        println!("✅  Received blockchain is valid. Replacing current blockchain with received blockchain");

        self.blocks = new_blocks
            .iter()
            .map(|json| {
                Block::new(json.index,
                           json.previous_hash.clone(),
                           json.timestamp,
                           json.data.clone(),
                           json.hash.clone(),
                           json.nonce)
            })
            .collect();

        Ok(())

        // println!("Received blockchain is invalid");
    }

    fn is_valid_chain(&mut self, chain: &Vec<Block>) -> Result<bool, Error> {
        if chain[0] != genesis() {
            return Err(Error::InvalidChain);
        }

        let mut temp_blocks = vec![&chain[0]];

        for i in 0..chain.len() {
            if self.is_valid_new_block(&chain[i], &temp_blocks[i - 1])? {
                temp_blocks.push(&chain[i])
            } else {
                return Err(Error::InvalidChain);
            }
        }

        Ok(true)
    }

    fn add_block(&mut self, new_block: Block) -> Result<bool, Error> {

        let latest_block = self.get_latest_block().clone();

        let mut error = true;
        let mut blocks = vec![];
        if self.is_valid_new_block(&new_block, &latest_block)? {
            blocks.push(new_block);
            error = false;
        }

        if !error {
            self.blocks.append(&mut blocks);

            return Ok(true);
        }



        Err(Error::InvalidBlock)
    }

    pub fn add_block_from_peer(&mut self, block: Block) -> Result<bool, Error> {

        // let block: Block = serde_json::from_value(json).unwrap();

        let latest_block = self.get_latest_block().clone();





        if self.is_valid_new_block(&block, &latest_block)? {

            self.blocks
                .append(&mut vec![Block::new(block.index,
                                             block.previous_hash,
                                             block.timestamp,
                                             block.data,
                                             block.hash,
                                             block.nonce)]);

            return Ok(true);
        }



        Err(Error::InvalidBlock)
    }

    fn calculate_hash_for_block(&self, block: &Block) -> String {
        self.calculate_hash(&block.index,
                            &block.previous_hash,
                            &block.timestamp,
                            &block.data,
                            &block.nonce)
    }

    fn calculate_hash(
        &self,
        index: &i64,
        previous_hash: &String,
        timestamp: &DateTime<Utc>,
        data: &String,
        nonce: &i64,
    ) -> String {

        let mut hasher = Sha3::sha3_256();

        let input_string = format!("{}{}{}{}{}",
                                   index.to_string(),
                                   previous_hash,
                                   timestamp.to_string(),
                                   data,
                                   nonce.to_string());

        hasher.input_str(&input_string);

        hasher.result_str().to_owned()
    }

    fn is_valid_new_block(&self, new_block: &Block, previous_block: &Block) -> Result<bool, Error> {

        let block_hash = self.calculate_hash_for_block(new_block);

        if previous_block.index + 1 != new_block.index {
            println!("❌  new block has invalid index");
            return Err(Error::InvalidIndex);
        } else if previous_block.hash != new_block.previous_hash {
            println!("❌  new block has invalid previous hash");
            return Err(Error::InvalidHash);
        } else if block_hash != new_block.hash {
            println!("❌  invalid hash: {} {}",
                     self.calculate_hash_for_block(&new_block),
                     new_block.hash);
            return Err(Error::InvalidHash);
        } else if !self.is_valid_hash_difficulty(self.calculate_hash_for_block(&new_block)) {
            println!("❌  invalid hash does not meet difficulty requirements: { }",
                     self.calculate_hash_for_block(&new_block));

            return Err(Error::HashNotDifficult);
        }
        Ok(true)
    }

    fn generate_next_block(&mut self, block_data: String) -> Block {
        let previous_block = self.get_latest_block();
        let next_index = previous_block.index + 1;
        let next_time_stamp = Utc.timestamp(Utc::now().timestamp(), 0);
        let mut nonce = 0;
        let mut next_hash = "".to_owned();

        while !self.is_valid_hash_difficulty(next_hash.clone()) {
            nonce += 1;

            next_hash = self.calculate_hash(&next_index,
                                            &previous_block.hash.clone(),
                                            &next_time_stamp,
                                            &block_data.clone(),
                                            &nonce);

        }


        Block::new(next_index,
                   previous_block.hash.clone(),
                   next_time_stamp,
                   block_data,
                   next_hash,
                   nonce)
    }

    fn is_valid_hash_difficulty(&self, hash: String) -> bool {

        let search_string = (self.difficulty + 1).to_string() + "0";

        hash.chars()
            .position(|x| x == search_string.chars().nth(0).unwrap()) == Some(0) &&
        hash.contains(&search_string)
    }
}
