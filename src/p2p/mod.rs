use serde_json::Value;
use serde_json;
use super::ws;
use super::ws::{Handler, Message, Sender};
use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashSet;
use blockchain::CHAIN;
use blockchain::BlockChain;

#[derive(Debug, Deserialize, Serialize)]
pub enum MessageType {
    QueryLatest,
    QueryAll,
    ResponseBlockchain,
    Mine,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PeerMessage {
    pub kind: MessageType,
    pub data: Option<Value>,
    pub seed: Option<String>,
}

type Peers = Rc<RefCell<HashSet<String>>>;

pub struct Server {
    pub peers: Peers,
    pub ip: Option<String>,
    pub out: Sender,
}

pub fn start_server(address: String) -> () {

    info!("{}", address);

    let peers = Peers::new(RefCell::new(HashSet::with_capacity(10_000)));

    // if let Err(error) =

    ws::listen(address, |out| {
        Server {
            out: out,
            ip: None,
            peers: peers.clone(),
        }

    })
            .unwrap();




    // {
    //     error!("Failed to create WebSocket due to {:?}", error);
    // }
}


impl Handler for Server {
    fn on_open(&mut self, _: ws::Handshake) -> ws::Result<()> {

        let chain = CHAIN.lock().unwrap();

        let chain = serde_json::to_string(&*chain).unwrap();
        try!(self.out
                 .broadcast(format!("{:?}",
                                    json!({
                                        "kind": "ResponseBlockchain",
                                         "data": chain
                                }))));
        Ok(())

    }
    fn on_message(&mut self, msg: Message) -> ws::Result<()> {

        match msg {
            Message::Text(x) => {
                info!("Recieved text message: {}.", x);

                let message: PeerMessage = serde_json::from_str(&x).unwrap();

                match message.kind {
                    MessageType::QueryLatest => {

                        let latest_chain = CHAIN.lock().unwrap().get_latest_block().clone();

                        let response = serde_json::to_string(&latest_chain).unwrap();

                        try!(self.out.send(response))
                    },

                    MessageType::QueryAll => {

                        let chain = CHAIN.lock().unwrap();

                        let response = serde_json::to_string(&*chain).unwrap();

                        try!(self.out.send(response))


                    },

                    MessageType::ResponseBlockchain => {
                        let mut received_chain: BlockChain =
                            serde_json::from_value(message.data.unwrap()).unwrap();

                        received_chain.blocks.sort();
                        let mut chain = CHAIN.lock().unwrap();

                        let latest_block_received = received_chain.get_latest_block().clone();

                        let latest_block_held = chain.get_latest_block().clone();

                        if latest_block_received.index <= latest_block_held.index {
                            info!("💤  Received latest block is not longer than current blockchain. Do nothing")
                        }

                        info!("🐢  Blockchain possibly behind. Received latest block is {}. Current latest block is {}.",
                              latest_block_received.index,
                              latest_block_held.index);


                        if latest_block_held.hash == latest_block_received.previous_hash {
                            info!("👍  Previous hash received is equal to current hash. Append received block to blockchain.");
                            match chain.add_block_from_peer(latest_block_received) {
                                Ok(_) => (),
                                Err(e) => {
                                    try!(self.out.send(format!("{:?}",
                                                        json!({
                                        "error": e,
                                }))))
                                },
                            };

                            let chain = serde_json::to_string(&*chain).unwrap();
                            try!(self.out
                                     .broadcast(format!("{:?}",
                                                        json!({
                                        "kind": "ResponseBlockchain",
                                        "data": chain
                                }))));
                        } else if received_chain.blocks.len() == 1 {
                            info!(
                                "🤔  Received previous hash different from current hash. Get entire blockchain from peer."
                            );
                            try!(self.out.broadcast(format!("{:?}", json!({
                                        "kind": "QueryAll",
                                    }))));
                        } else {
                            info!("⛓  Peer blockchain is longer than current blockchain.");

                            match chain.replace_chain(received_chain.blocks) {
                                Ok(_) => (),
                                Err(e) => {
                                    try!(self.out.send(format!("{:?}",
                                                        json!({
                                        "error": e,
                                }))))
                                },
                            };


                            let chain = serde_json::to_string(&*chain).unwrap();
                            try!(self.out.broadcast(format!("{:?}", json!({
                                        "kind": "ResponseBlockchain",
                                         "data": chain
                                }))));
                        }

                    },

                    MessageType::Mine => {

                        let seed = message.seed.unwrap_or(" ".to_owned());

                        let mut chain = CHAIN.lock().expect("Locked");


                        match chain.mine(seed) {
                                Ok(_) => (),
                                Err(e) => {
                                    try!(self.out.send(format!("{:?}",
                                                        json!({
                                        "error": e,
                                }))))
                                },
                        };

                        let chain = serde_json::to_string(&*chain).unwrap();

                        try!(self.out.broadcast(format!("{:?}", json!({
                                         "kind": "ResponseBlockchain",
                                         "data": chain
                                }))));
                    },
                }

            },

            _ => error!("This shouldn't happen"),
        }



        Ok(())

    }

    fn on_close(&mut self, _: ws::CloseCode, _: &str) {
        if let Some(ip) = self.ip.as_ref() {
            self.peers.borrow_mut().remove(ip);

            let message = format!("{} has left the chat.", ip);
            if let Err(err) = self.out
                   .send(format!("{:?}", json!({
                "path": "/left",
                "content": message,
            }))) {
                error!("{:?}", err);
            }
        }
    }
}
